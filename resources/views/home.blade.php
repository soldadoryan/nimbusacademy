<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Nimbus Academy</title>
  <!-- font -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:100" rel="stylesheet">
  <!-- font -->
  <link rel="stylesheet" href="/css/bootstrap.css">
  <link rel="stylesheet" href="/css/style.css">
</head>
<body>
  <div class="menu">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <h1>Nimbus <span>Academy</span></h1>
        </div>
        <div class="col-lg-4 col-lg-offset-5">
          <ul class="list-inline">
            <li><a href=""><i class="fa fa-chevron-circle-right"></i> Entrar</a></li>
            <li><a href=""><i class="fa fa-sign-in"></i></i> Cadastrar</a></li>
            <li></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="banner">
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-lg-offset-1 text-center">
          <h4>"Nosso objetivo é que qualquer pessoa que queira, tenha a chance de aprender a programar!"</h4>
          <button class="btn btn-primary btn-lg">Como me tornar um membro da Nimbus?</button>
        </div>
      </div>
    </div>
  </div>
  <div class="conteudo">
    <div class="container">
      <div class="divisor-50"></div>
      <div class="row">
        <div class="col-lg-6">
          <h2><i class="fa fa-calendar-check-o "></i> Eventos</h2>
        </div>
        <div class="col-lg-6">
          <button class="btn btn-primary btn-lg">Ver Mais</button>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-4">
        </div>
      </div>
    </div>
  </div>

  <script src="https://use.fontawesome.com/88f58b9255.js"></script>
</body>
</html>
